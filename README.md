Backend Skeleton
================

short intro

Installation
------------

Get the source code:

```
$ git clone ssh://git@bitbucket.almedso.de:7999/almedso/cosmea-skeleton.git
```

```
$ npm install
```
### Run linting (using jshint)

```
$ npm run jshint
```

### Run the tests

```
$ npm test
```
The output is stored at stdout.
see https://mochajs.org/#getting-started for details.

### Build the developer documentation

```
$ npm run doc
```
The output is found at *build/doc/xxx/<version>/index.html*

