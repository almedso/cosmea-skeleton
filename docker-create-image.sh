#!/bin/bash
set -e

##
# This script creates a docker image in various flavors
#
# @param (string) type: one of the following:
#  DEV, dev, DEVELOPMENT, development 
#     creates a development image (enriched with
#     tools development code, etc.)
#  PROD, prod, PRODUCTION, production

# set the default to prod
MODE=${1:-prod}

# get image name and version from package.json
NAME=$(node handle-pjson.js name)

# patch and get version from package.json
if [ $MODE == "dev" ]; then
  node handle-pjson.js patch
  NAME=${NAME}-dev
fi
VERSION=$(node handle-pjson.js version)

# create the image
if [ $MODE == "dev" ]; then
  DOCKERFILE=Dockerfile
else
  # minimal production image
  DOCKERFILE=Dockerfile.build 
fi
docker build --tag ${NAME}:${VERSION} .

# (optinally) publish the docker image

# cleanup (i.e. get package back to previous content)
# node handle-pjson.js unpatch not implemented yet
git checkout -- package.json # must not be changed before
