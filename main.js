/**
 * @copyright (c) 2017 almedso GmbH
 */
/* global module */
'use strict';
var express = require('express');
var exports = module.exports = {};

var xxxAPI = require('./src/api/xxx');


/** 
 * Setup routes and open database
 * 
 * @param (Object) app application object created via express
 */
exports.initXxx = function ( app ) {

  app.use('/api-skeleton', xxxAPI);

};