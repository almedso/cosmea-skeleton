var child_process = require('child_process');
var fs = require('fs');
var pjson = require('./package.json');

/**
 * Determine the pre-release portion of the version (synchronous function)
 *
 * The pre-release portion is used to to trace back to the source code in git
 * where the package was build from.
 *
 * @returns {String} empty in case of an error, otherwise the pre-release portion
 *
 */
function determinePreReleasePortion () {
  const regex = /-[0-9]+-g[0-9a-f]+/;
  var portion = '';
  var gitDescribe = '';
  try {
   gitDescribe =  child_process.execSync('git describe', {stdio: ['ignore', 'pipe', 'ignore']}).toString();
   portion = gitDescribe.match(regex)[0] || '';
  } catch (e) {
   // e.g. git describe delivers exit code != 0 (i.e. no tag set
   portion = '';
  }
  return portion;
}


/**
 * Determine the build portion of the version, that is the current date.
 *
 * This is a synchronous function.
 *
 * @returns {String} '+yyyymmdd' whereby y is the year, m is the month d is the day
 *
 */ 
function determineBuildPortion() {
  var today = new Date();

  function pad(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }

  return '+' + today.getUTCFullYear() + pad(today.getUTCMonth() + 1) + pad(today.getUTCDate()); 
}
/**
 *
 * Read base portion of the version from package.json
 *
 * The base portion is composed of major.minor.patch
 *
 * @returns {String} the base portion of the version if not set it 
 *   returns '0.0.0'
 *
 */
function determineBasePortion() {
  const regex = /^([0-9]+\.[0-9]+\.[0-9]).*$/
  var version = pjson.version || '0.0.0';
  return version.match(regex)[1];
}


// make promise version of fs.readFile()
fs.readFileAsync = function(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename, function(err, data){
            if (err) 
                reject(err); 
            else 
                resolve(data);
        });
    });
};

	
fs.writeFileAsync = function(filename, data) {
  return new Promise(function(resolve, reject) {
    fs.writeFile(filename, data, 'utf-8', function(err) {
      if (err) reject(err);
      else resolve(data);
    });
  });
};


/**
 *  Punches in the version into package.json file
 *
 *  @param {String} fullVersion the string that contains the complete semantic version.
 *  @returns {Promise} resloves if version punch in was successful otherwise rejects
 *
 */
function punchInVersion(fullVersion) {
  const packageFileName = 'package.json';

  return fs.readFileAsync(packageFileName)
  .then( (data) => {
    lines = data.toString().split('\n');
    return lines.map((line) => { 
      if ( line.includes('version') ) { line = '  "version": "' + fullVersion + '",'; }
      return line;
    }).join('\n');
  })
  .then( (data) => fs.writeFileAsync(packageFileName, data));
}

var exports = module.exports = {};

/**
  * Patch the version information inside package.json
  *
  * The version is determined as follow:
  *
  * - follow semantic versioning (decision at meeting on 2017-05-24)
  * - major.minor.patch is taken from the version from package.json 
  * - "prerelease extension" will be the output from git describe; this
  *   requires at least a single tag to be set.
  * - the "build extension" will be "yyyymmdd" just for convenience
  *
  * The version patched only if the environment variable PATCH_VERSION has
  * a value that evaluates in javascript to true
  *
  * @returns {Promise} The promise resolves if the version was patched
  *   sucessfully and it rejects if patching failed for whatever reason
  */
exports.patchVersion = function () {
   const PATCH_VERSION = process.env.PATCH_VERSION;
   if (PATCH_VERSION) {
     var newVersion = determineBasePortion() + determinePreReleasePortion() + determineBuildPortion();  
     console.log(newVersion); // print the version to stdout
     return punchInVersion(newVersion);
   } else {
     console.log(determineBasePortion());
     return Promise.resolve(true);
   }
};



const args = process.argv;
switch (args[args.length - 1 ]) {
  case 'patch':
    var newVersion = determineBasePortion() + determinePreReleasePortion();
    punchInVersion(newVersion);
    break;
  case 'name':
    console.log(pjson.name);
    break;
  case 'version':
    console.log(pjson.version);
    break;

}



