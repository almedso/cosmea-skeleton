FROM node:latest
# this docker file is to deploy a development image of the server portion

# Create user and  app directory (user home)
RUN useradd -ms /bin/bash -d /var/lib/ops-be-core ops-be-core

WORKDIR /var/lib/ops-be-core

# Install app dependencies
COPY package.json /var/lib/ops-be-core
RUN npm install

# Bundle app source (server side only)
COPY src /var/lib/ops-be-core
COPY main.js /var/lib/ops-be-core

# Copy the client application(s) for testing purposes
# COPY frontend/dist /var/lib/pims/frontend

# Copy arbitrary build specs and configs
COPY LICENSE /var/lib/ops-be-core
COPY Gruntfile.js /var/lib/ops-be-core
COPY .jshintrc /var/lib/ops-be-core
COPY jsdocconf.json /var/lib/ops-be-core
COPY .jshintrc /var/lib/ops-be-core
COPY docker-cmd.sh /var/lib/ops-be-core

EXPOSE 3000
USER ops-be-core

CMD [ "/var/lib/ops-be-core/docker-cmd.sh" ]
