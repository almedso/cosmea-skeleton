#!/bin/bash
set -e

# lint
./node_modules/grunt/bin/grunt jshint:local

# test
./node_modules/grunt/bin/grunt mochaTest:local

# docu
./node_modules/grunt/bin/grunt jsdoc
./node_modules/grunt/bin/grunt apidoc
