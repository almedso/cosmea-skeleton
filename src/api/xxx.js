/**
 *
 * @fileOverview REST role API
 * @copyright (C) 2017, Volker Kempert
 *
 */

/* jshint -W002 */
'use strict';
var debug = require('debug')('api');
var logger = require('winston');
var ObjectId = require('mongoose').Types.ObjectId;
var express = require('express');

var Xxx = require('../models/xxx').Xxx;

var exports = module.exports = express.Router();


exports.get('/xxxs', function(req, res) {
  debug('allXxx');
  Xxx.find({}, function(err, xxx) { res.json(xxx); });
});


/**
 * @apiDefine loggedIn
 * @apiHeader content-type aplication/json.
 * @apiHeader x-access-token JWS authentication token (bearer).
 * @apiHeaderExample {json} Header-Example
 *   {
 *     "Content-Type": "application/json"
 *     "x-access-token": "bearer the-JWS-token-string"
 *   }
 */

/**
 * @api {post} /role/ Create new Xxx
 * @apiName xxx (post)
 * @apiGroup Xxx
 * 
 * @apiUse loggedIn
 * @apiDescription Creates a new Xxx
 * 
 * @apiParam {String} name The name of the xxx.
 * 
 * @apiParamExample {json} Request-Example:
 *     {
 *       "name": "myXxx",
 *     }
 *
 * @apiSuccess (201) {Object} xxx Xxx Object that is created in json format.
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "name": "foo",
 *       "_id": "12345678901234567890123456789012",
 *       "orgId": "12345678901234567890123456789012"
 *     }
 *
 * @apiError (400) {string} 400 Xxx creation error.
 * @apiError (409) {string} 409 Xxx with that name already exists in the organization.
 * @apiError (503) {string} 503 Database not available.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "XxxNotFound"
 *     }
 */
exports.post('/xxx', function(req, res) {

    new Xxx({ name : req.body.name, }).save()
    .then( (xxx, err) =>  {
      if (err) {
        logger.error("createXxx", {error: err});
        res.status(400).json(err);
      } else {
        logger.info("Xxx created", {xxxId: xxx._id});
        res.status(201).json(xxx);
      }
    });
});


