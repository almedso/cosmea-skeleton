/**
 *
 * @fileOverview Tests of api roles.
 * @copyright (C) 2017 Volker Kempert
 *
 */
'use strict';
var expect = require('chai').expect;
var request = require('supertest');

var globalSpec = require('../global_spec.util');  // global test helper functions

var logger = require('winston');
var Xxx = require('../models/xxx').Xxx;

// load environment after environment variables are manipulated
var sut = require('../sut_spec.util').sut;
var agent = request(sut.app);


describe('API Xxx object calls', function() {

  let apiUrl = '/api-skeleton';

  before('Open Database', globalSpec.openDatabase);

  after('Drop table and close', globalSpec.closeAndDropDatabase);

  describe('GET xxx', function() {

    before( 'Create xxx', function() {
      var xxx = new Xxx({
        name: 'foo',
      });
      return xxx.save();
    });

    it('should respond with a xxx list', function(done) {
      agent.get(apiUrl + '/xxxs')
      .set('Content-Type', 'application/json')
      .end(function(err, res) {
        expect(err).to.be.a('null');
        expect(res.status).to.be.equal(200);
        expect(res.body).to.be.an('array');
        expect(res.body).to.have.lengthOf(1);
        done();
        });
    });
  });


  describe('POST xxx/create', function() {

    it('should respond with 201 for new test xxx', function(done) {
      agent.post(apiUrl + '/xxx')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send({
        name: 'test',
      })
      .end(function(err, res) {
        expect(err).to.be.a('null');
        expect(res.status).to.be.equal(201);

        Xxx.findOne({name: 'test'},
        function(err, xxx) {
          expect(err).to.be.a('null');
          expect(xxx.name).to.be.equal('test');
          done();
        });
      });
    });
  });

});
