/**
 *
 * @fileOverview Domain model of the xxx object.
 * @copyright (C) 2017, Volker Kempert
 * @module models/role
 *
 */

'use strict';
var exports = module.exports = {};
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;


/**
 * Mongoose schema for a role
 *
 *
 *  note: an '' empty string as default for a required field
 *        causes the mongoose save function to fail
 *
 */
var xxxSchema = new Schema({
  /** name: Name of object acts as identifier */
  name: { type: String, required: true},
});

/**
 * Mongoose compiled model representing a role.
 * @class Role
 */
exports.Xxx = mongoose.model('Xxx', xxxSchema);
