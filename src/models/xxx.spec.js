/**
 *
 * @fileOverview Tests of the role object.
 * @copyright (C) 2017, Volker Kempert
 *
 */
/* global describe */
/* global it */
/* global xit */
/* global beforeEach */
/* global afterEach */
'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var expect = require('chai').expect;

var globalSpec = require('../global_spec.util');  // global test helper functions
var Xxx = require('./xxx').Xxx;


describe('Xxx Model', function() {

  before('Open Database', globalSpec.openDatabase);

  after('Drop table and close', globalSpec.closeAndDropDatabase);

  describe('New XXX', function() {

    it('Creates and saves a minimal xxx object', function () {
      const xxx = new Xxx({
        name: 'foo',
      });
      expect(xxx.name).to.be.equal("foo");
      return xxx.save();  // make it persistent
    });

  });
});
