'use strict';

var testDB = process.env.DATABASE || 'mongodb://localhost:27017/cosmea';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var logger = require('winston');

var exports =  module.exports = {};

exports.redirectLog = function() {
  // while testing, log only to file, leaving stdout free for unit test status messages
  // in real prododuction logging is to stdout and stderr like modern
  // systemd daemons (and docker) can conveniently handle
  logger.add(logger.transports.File, { filename: "./pms-test.log" });
  logger.remove(logger.transports.Console);
};

exports.redirectLog();  // make sure it is called once the file is required.

exports.openDatabase = function(done) {
  // in theory the database should be already connected by the
  // subject under tests via superagent/supertest
  // 1 == connected; 0 == disconnected; 2 == connecting; 3 == disconnecting
  if (mongoose.connection.readyState === 0 ) { 
    mongoose.connect(testDB, {
      useMongoClient: true,
      keepAlive: 120,
      connectTimeoutMS: 3000 
    });
  }
  mongoose.connection
  .once('open', () => { done(); })
  .on('error', (err) => {
    logger.warn('Problem connecting to mongo: ', err);
    done();
  })
  .on('connected', () => { logger.info('Connected to MongoDB: ', testDB) })
  .on('connecting', () => { logger.info('Connecting to MongoDB') })
  .on('disconnected', () => { logger.info('Disconnected from MongoDB') })
  .on('disconnecting', () => { logger.info('Disconnecting from MongoDB') })
  ;
};

exports.closeAndDropDatabase = function(done) {
  mongoose.connection.db.dropDatabase( function () {
    mongoose.disconnect(done);
  });
};
