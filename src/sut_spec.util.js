#!/bin/env node
/**
 * @fileOverview Subject Under Test nodesjs Server
 * @copyright (C) 2017, Volker Kempert
 *
 */

'use strict';
var express = require('express');
var bodyParser = require('body-parser');

var initXxx = require('../main').initXxx;


var Application = function () {

    //  Scope.
    var self = this;

    self.terminator = function(sig){
        database.close();
    };


    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     *
     *  Note: for testing purposes it is expected that the database is opend somewhere else.
     *
     */
    self.initializeApplication = function() {
        self.app = express();

        // use body parser middleware for handling all api requests
        // as a consequence all requests implementations can access to
        // body (json formatted) vars via req.body.xyz
        self.app.use(bodyParser.urlencoded({ extended: false }));
        self.app.use(bodyParser.json());

        // initialize module functionality
        initXxx( self.app ); 

        // Handle 404 error
        self.app.use(function(req, res, next) {
          if(req.accepts('html') && res.status(404)) {
            res.setHeader('Content-Type', 'application/json');
            res.send('{ "error": "Page not found" }');
            return;
          }
          next();
        });

        // Handle rest as internal server error: 500
        self.app.use(function(error, req, res, next) {
          res.setHeader('Content-Type', 'application/json');
          res.status(500).send('{ "error": "Internal Server Error" }');
        });

    };


};   /*  Application.  */

module.exports = { sut: new Application() };
module.exports.sut.initializeApplication();
