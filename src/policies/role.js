/**
 *
 * @fileOverview Policies for role object.
 * @copyright (C) 2017, Volker Kempert
 * @module policies/role
 *
 */

/* global module */
'use strict';

module.exports.rolePolicies = {
  manageRoleDenyPermissionPropagation: function () {
    return false;
  }

};
